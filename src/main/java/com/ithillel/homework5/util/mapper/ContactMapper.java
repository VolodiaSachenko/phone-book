package com.ithillel.homework5.util.mapper;

import com.ithillel.homework5.entity.Contact;
import com.ithillel.homework5.dto.ContactDto;

public final class ContactMapper {

    public static ContactDto toDto(Contact contact) {
        ContactDto dto = new ContactDto();
        dto.setId(contact.getId());
        dto.setName(contact.getName());
        dto.setSurname(contact.getSurname());
        dto.setPhoneNumber(contact.getPhoneNumber());
        return dto;
    }

    public static Contact fromDto(ContactDto contactDto) {
        Contact contact = new Contact();
        contact.setId(contactDto.getId());
        contact.setName(contactDto.getName());
        contact.setSurname(contactDto.getSurname());
        contact.setPhoneNumber(contactDto.getPhoneNumber());
        return contact;
    }
}
