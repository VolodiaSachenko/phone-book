<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add address</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 style="position: relative; left: 35%">Add address</h1>
    <form:form action="add/create" method="post" modelAttribute="address"
               cssStyle="width: 500px;position: relative;left: 30%;">
        <div class="mb-3">
            <form:hidden path="id" cssClass="form-control"/>
        </div>
        <div class="mb-3">
            <form:label path="country" cssClass="form-label">Country:</form:label>
            <form:input path="country" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="city" cssClass="form-label">City:</form:label>
            <form:input path="city" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="street" cssClass="form-label">Street:</form:label>
            <form:input path="street" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="zipCode" cssClass="form-label">Zip Code:</form:label>
            <form:input path="zipCode" cssClass="form-control" required="required"/>
        </div>
        <form:button class="btn btn-primary">Add</form:button><br><br>
        <div style="text-align: left">
            <a href="http://localhost:8080/">Return to home page</a>
        </div>
    </form:form>
</div>
</body>
</html>
