package com.ithillel.homework5.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class PhoneBookDto {
    private Long id;
    private ContactDto contact;
    private AddressDto address;
}
