package com.ithillel.homework5.dao;

import com.ithillel.homework5.entity.PhoneBook;

import java.util.List;

public interface PhoneBookDao {
    boolean save(PhoneBook phoneBook);

    PhoneBook findById(Long id);

    List<PhoneBook> findAll();

    boolean update(PhoneBook update);

    boolean delete(PhoneBook phoneBook);
}
