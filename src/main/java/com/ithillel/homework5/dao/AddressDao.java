package com.ithillel.homework5.dao;

import com.ithillel.homework5.entity.Address;

import java.util.List;

public interface AddressDao {
    boolean save(Address address);

    Address findById(Long id);

    List<Address> findAll();

    boolean update(Address update);

    boolean delete(Address address);
}
