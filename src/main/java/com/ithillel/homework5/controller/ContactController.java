package com.ithillel.homework5.controller;

import com.ithillel.homework5.service.ContactService;
import com.ithillel.homework5.dto.ContactDto;
import com.ithillel.homework5.util.mapper.ContactMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ContactController {
    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @RequestMapping(value = {"/contacts"}, method = RequestMethod.GET)
    public String getAll(Model model) {
        List<ContactDto> dto = contactService
                .readAll()
                .stream()
                .map(ContactMapper::toDto)
                .collect(Collectors.toList());
        model.addAttribute("contacts", dto);
        return "contacts";
    }

    @RequestMapping(value = {"contact/add"}, method = RequestMethod.GET)
    public String addContactForm(Model model) {

        model.addAttribute("contact", new ContactDto());
        return "addContact";
    }

    @RequestMapping(value = {"contact/add/create"}, method = RequestMethod.POST)
    public ModelAndView createContact(@ModelAttribute("contact") ContactDto contactDto) {

        contactService.create(ContactMapper.fromDto(contactDto));
        return new ModelAndView(new RedirectView("/contacts"));
    }

    @RequestMapping(value = {"contact/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {

        model.addAttribute("updateContact",
                ContactMapper.toDto(contactService.findById(Long.parseLong(id))));
        return "updateContact";
    }

    @RequestMapping(value = {"contact/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updateContact(@ModelAttribute("update") ContactDto contactDto) {

        contactService.update(ContactMapper.fromDto(contactDto));
        return new ModelAndView(new RedirectView("/contacts"));
    }

    @RequestMapping(value = {"contact/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteContact(@PathVariable String id) {

        contactService.delete(contactService.findById(Long.parseLong(id)));
        return new ModelAndView(new RedirectView("/contacts"));
    }
}
