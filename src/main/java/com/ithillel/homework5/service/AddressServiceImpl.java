package com.ithillel.homework5.service;

import com.ithillel.homework5.dao.JdbcAddressDao;
import com.ithillel.homework5.entity.Address;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    private final JdbcAddressDao jdbcAddressDao;

    public AddressServiceImpl(JdbcAddressDao jdbcAddressDao) {
        this.jdbcAddressDao = jdbcAddressDao;
    }

    @Override
    public boolean create(Address address) {
        return jdbcAddressDao.save(address);
    }

    @Override
    public Address findById(Long id) {
        return jdbcAddressDao.findById(id);
    }

    @Override
    public List<Address> readAll() {
        return jdbcAddressDao.findAll();
    }

    @Override
    public boolean update(Address address) {
        return jdbcAddressDao.update(address);
    }

    @Override
    public boolean delete(Address address) {
        return jdbcAddressDao.delete(address);
    }
}
