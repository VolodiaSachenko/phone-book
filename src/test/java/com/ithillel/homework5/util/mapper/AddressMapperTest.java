package com.ithillel.homework5.util.mapper;

import com.ithillel.homework5.entity.Address;
import com.ithillel.homework5.dto.AddressDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressMapperTest {

    @Test
    void toDto() {
        Address address = new Address();
        address.setId(1L);
        address.setCountry("USA");
        address.setCity("LA");
        address.setStreet("Test street");
        address.setZipCode("1234d");

        AddressDto addressDto = AddressMapper.toDto(address);

        assertEquals(address.getId(), addressDto.getId());
        assertEquals(address.getCountry(), addressDto.getCountry());
        assertEquals(address.getCity(), addressDto.getCity());
        assertEquals(address.getStreet(), addressDto.getStreet());
        assertEquals(address.getZipCode(), addressDto.getZipCode());
    }

    @Test
    void fromDto() {
        AddressDto addressDto = new AddressDto();
        addressDto.setId(1L);
        addressDto.setCountry("USA");
        addressDto.setCity("LA");
        addressDto.setStreet("Test street");
        addressDto.setZipCode("1234d");

        Address address = AddressMapper.fromDto(addressDto);

        assertEquals(addressDto.getId(), address.getId());
        assertEquals(addressDto.getCountry(), address.getCountry());
        assertEquals(addressDto.getCity(), address.getCity());
        assertEquals(addressDto.getStreet(), address.getStreet());
        assertEquals(addressDto.getZipCode(), address.getZipCode());
    }
}