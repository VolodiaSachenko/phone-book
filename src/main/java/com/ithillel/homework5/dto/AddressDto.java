package com.ithillel.homework5.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class AddressDto {
    private Long id;
    private String country;
    private String city;
    private String street;
    private String zipCode;

    @Override
    public String toString() {
        return id.toString();
    }
}
