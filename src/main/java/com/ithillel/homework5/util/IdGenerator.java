package com.ithillel.homework5.util;

import org.springframework.jdbc.core.JdbcTemplate;

public final class IdGenerator {

    public static Long generateId(JdbcTemplate jdbcTemplate, String query) {
        Long max = 1L;
        if (jdbcTemplate.queryForObject(query, Long.class) != null) {
            max += jdbcTemplate.queryForObject(query, Long.class);
        }
        return max;
    }
}
