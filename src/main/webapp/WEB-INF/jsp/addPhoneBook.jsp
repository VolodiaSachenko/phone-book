<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add new phone book</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 style="position: relative; left: 35%">Add new phone book contact</h1>
    <form:form action="add/create" method="post" modelAttribute="phoneBook"
               cssStyle="width: 500px;position: relative;left: 30%;">
        <div class="mb-3">
            <form:label path="contact.id" cssClass="form-label">Contact id:</form:label>
            <form:select cssClass="form-control" path="contact.id" items="${contacts}"/>
        </div>
        <div class="mb-3">
            <form:label path="address.id" cssClass="form-label">Address id:</form:label>
            <form:select cssClass="form-control" path="address.id" items="${addresses}"/>
        </div>
        <div class="mb-3">
            <form:hidden path="id" cssClass="form-control"/>
        </div>
        <form:button class="btn btn-primary">Add</form:button><br><br>
        <div style="text-align: left">
            <a href="http://localhost:8080/">Return to home page</a>
        </div>
    </form:form>
</div>
</body>
</html>
