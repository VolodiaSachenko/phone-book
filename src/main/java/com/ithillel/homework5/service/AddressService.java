package com.ithillel.homework5.service;

import com.ithillel.homework5.entity.Address;

import java.util.List;

public interface AddressService {
    boolean create(Address address);

    Address findById(Long id);

    List<Address> readAll();

    boolean update(Address update);

    boolean delete(Address address);
}
