package com.ithillel.homework5.util.mapper;

import com.ithillel.homework5.entity.Contact;
import com.ithillel.homework5.dto.ContactDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContactMapperTest {

    @Test
    void toDto() {
        Contact contact = new Contact();
        contact.setId(1L);
        contact.setName("Test");
        contact.setSurname("Surname");
        contact.setPhoneNumber("123");

        ContactDto contactDto = ContactMapper.toDto(contact);

        assertEquals(contact.getId(), contactDto.getId());
        assertEquals(contact.getName(), contactDto.getName());
        assertEquals(contact.getSurname(), contactDto.getSurname());
        assertEquals(contact.getPhoneNumber(), contactDto.getPhoneNumber());
    }

    @Test
    void fromDto() {
        ContactDto contactDto = new ContactDto();
        contactDto.setId(1L);
        contactDto.setName("Test");
        contactDto.setSurname("Surname");
        contactDto.setPhoneNumber("123");

        Contact contact = ContactMapper.fromDto(contactDto);

        assertEquals(contactDto.getId(), contact.getId());
        assertEquals(contactDto.getName(), contact.getName());
        assertEquals(contactDto.getSurname(), contact.getSurname());
        assertEquals(contactDto.getPhoneNumber(), contact.getPhoneNumber());
    }
}