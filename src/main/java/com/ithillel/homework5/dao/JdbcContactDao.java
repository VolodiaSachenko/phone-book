package com.ithillel.homework5.dao;

import com.ithillel.homework5.entity.Contact;
import com.ithillel.homework5.util.IdGenerator;
import com.ithillel.homework5.util.rowmapper.ContactRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class JdbcContactDao implements ContactDao {
    private final JdbcTemplate jdbcTemplate;

    public JdbcContactDao(JdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcTemplate.setDataSource(dataSource);
    }

    @Override
    public boolean save(Contact contact) {
        String query = "INSERT INTO homework_db.book.contacts (id, name, surname, phone) " +
                "VALUES (?,?,?,?)";

        return jdbcTemplate.update(query,
                IdGenerator.generateId(jdbcTemplate, "SELECT MAX(id) FROM homework_db.book.contacts"),
                contact.getName(),
                contact.getSurname(),
                contact.getPhoneNumber()) == 1;
    }

    @Override
    public Contact findById(Long id) {
        String query = "SELECT * FROM homework_db.book.contacts WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new ContactRowMapper(), id);
    }

    @Override
    public List<Contact> findAll() {
        String query = "SELECT * FROM homework_db.book.contacts";
        return jdbcTemplate.query(query, new ContactRowMapper());
    }

    @Override
    public boolean update(Contact update) {
        String query = "UPDATE homework_db.book.contacts " +
                "SET name = ?, surname = ?, phone = ? WHERE id = ?";

        return jdbcTemplate.update(query,
                update.getName(),
                update.getSurname(),
                update.getPhoneNumber(),
                update.getId()) == 1;
    }

    @Override
    public boolean delete(Contact contact) {
        String query = "DELETE FROM homework_db.book.contacts WHERE id = ?";
        return jdbcTemplate.update(query, contact.getId()) == 1;
    }
}
