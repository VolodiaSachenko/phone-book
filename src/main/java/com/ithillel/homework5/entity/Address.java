package com.ithillel.homework5.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class Address {
    private Long id;
    private String country;
    private String city;
    private String street;
    private String zipCode;

    public Address() {
        this.zipCode = "";
    }

    public Address(Long id, String country, String city, String streetAddress, String zipCode) {
        this.id = id;
        this.country = country;
        this.city = city;
        this.street = streetAddress;
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", streetAddress='" + street + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return id.equals(address.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
