package com.ithillel.homework5.dao;

import com.ithillel.homework5.entity.Contact;

import java.util.List;

public interface ContactDao {
    boolean save(Contact contact);

    Contact findById(Long id);

    List<Contact> findAll();

    boolean update(Contact update);

    boolean delete(Contact contact);
}
