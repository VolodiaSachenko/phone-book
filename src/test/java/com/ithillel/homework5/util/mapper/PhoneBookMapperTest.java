package com.ithillel.homework5.util.mapper;

import com.ithillel.homework5.entity.PhoneBook;
import com.ithillel.homework5.dto.AddressDto;
import com.ithillel.homework5.dto.ContactDto;
import com.ithillel.homework5.dto.PhoneBookDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PhoneBookMapperTest {

    @Test
    void toDto() {
        ContactDto contactDto = new ContactDto();
        contactDto.setId(1L);
        contactDto.setPhoneNumber("123");
        contactDto.setName("Test");
        contactDto.setSurname("Test surname");

        AddressDto addressDto = new AddressDto();
        addressDto.setId(1L);
        addressDto.setCountry("UA");
        addressDto.setCity("Kyiv");
        addressDto.setStreet("Test street");
        addressDto.setZipCode("1123");

        PhoneBook phoneBook = new PhoneBook();
        phoneBook.setId(1L);
        phoneBook.setContact(1L);
        phoneBook.setAddress(1L);

        PhoneBookDto phoneBookDto = PhoneBookMapper.toDto(phoneBook, contactDto, addressDto);

        assertEquals(phoneBook.getId(), phoneBookDto.getId());
        assertEquals(phoneBook.getContact(), phoneBookDto.getContact().getId());
        assertEquals(phoneBook.getAddress(), phoneBookDto.getAddress().getId());
    }

    @Test
    void toDtoUpdate() {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.setId(1L);
        phoneBook.setContact(1L);
        phoneBook.setAddress(1L);

        PhoneBookDto phoneBookDto = PhoneBookMapper.toDtoUpdate(phoneBook);

        assertEquals(phoneBook.getId(), phoneBookDto.getId());
        assertEquals(phoneBook.getContact(), phoneBookDto.getContact().getId());
        assertEquals(phoneBook.getAddress(), phoneBookDto.getAddress().getId());
    }

    @Test
    void fromDto() {
        ContactDto contactDto = new ContactDto();
        contactDto.setId(1L);
        contactDto.setPhoneNumber("123");
        contactDto.setName("Test");
        contactDto.setSurname("Test surname");

        AddressDto addressDto = new AddressDto();
        addressDto.setId(1L);
        addressDto.setCountry("UA");
        addressDto.setCity("Kyiv");
        addressDto.setStreet("Test street");
        addressDto.setZipCode("1123");

        PhoneBookDto phoneBookDto = new PhoneBookDto();
        phoneBookDto.setId(1L);
        phoneBookDto.setAddress(addressDto);
        phoneBookDto.setContact(contactDto);

        PhoneBook phoneBook = PhoneBookMapper.fromDto(phoneBookDto);

        assertEquals(phoneBookDto.getId(), phoneBook.getId());
        assertEquals(phoneBookDto.getAddress().getId(), phoneBook.getId());
        assertEquals(phoneBookDto.getContact().getId(), phoneBook.getId());
    }
}