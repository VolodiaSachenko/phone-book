package com.ithillel.homework5.service;

import com.ithillel.homework5.entity.Contact;

import java.util.List;

public interface ContactService {
    boolean create(Contact contact);

    Contact findById(Long id);

    List<Contact> readAll();

    boolean update(Contact update);

    boolean delete(Contact contact);
}
