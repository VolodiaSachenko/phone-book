package com.ithillel.homework5.service;

import com.ithillel.homework5.dao.JdbcPhoneBookDao;
import com.ithillel.homework5.entity.PhoneBook;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneBookServiceImpl implements PhoneBookService {
    private final JdbcPhoneBookDao jdbcPhoneBookDao;

    public PhoneBookServiceImpl(JdbcPhoneBookDao jdbcPhoneBookDao) {
        this.jdbcPhoneBookDao = jdbcPhoneBookDao;
    }

    @Override
    public boolean create(PhoneBook phoneBook) {
        return jdbcPhoneBookDao.save(phoneBook);
    }

    @Override
    public PhoneBook findById(Long id) {
        return jdbcPhoneBookDao.findById(id);
    }

    @Override
    public List<PhoneBook> readAll() {
        return jdbcPhoneBookDao.findAll();
    }

    @Override
    public boolean update(PhoneBook phoneBook) {
        return jdbcPhoneBookDao.update(phoneBook);
    }

    @Override
    public boolean delete(PhoneBook phoneBook) {
        return jdbcPhoneBookDao.delete(phoneBook);
    }
}
