package com.ithillel.homework5.controller;

import com.ithillel.homework5.service.AddressService;
import com.ithillel.homework5.dto.AddressDto;
import com.ithillel.homework5.util.mapper.AddressMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AddressController {
    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @RequestMapping(value = {"/addresses"}, method = RequestMethod.GET)
    public String getAll(Model model) {
        List<AddressDto> dto = addressService
                .readAll()
                .stream()
                .map(AddressMapper::toDto)
                .collect(Collectors.toList());
        model.addAttribute("addresses", dto);
        return "addresses";
    }

    @RequestMapping(value = {"address/add"}, method = RequestMethod.GET)
    public String addAddressForm(Model model) {

        model.addAttribute("address", new AddressDto());
        return "addAddress";
    }

    @RequestMapping(value = {"address/add/create"}, method = RequestMethod.POST)
    public ModelAndView createAddress(@ModelAttribute("address") AddressDto addressDto) {

        addressService.create(AddressMapper.fromDto(addressDto));
        return new ModelAndView(new RedirectView("/addresses"));
    }

    @RequestMapping(value = {"address/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {

        model.addAttribute("updateAddress",
                AddressMapper.toDto(addressService.findById(Long.parseLong(id))));
        return "updateAddress";
    }

    @RequestMapping(value = {"address/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updateAddress(@ModelAttribute("update") AddressDto addressDto) {

        addressService.update(AddressMapper.fromDto(addressDto));
        return new ModelAndView(new RedirectView("/addresses"));
    }

    @RequestMapping(value = {"address/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteAddress(@PathVariable String id) {

        addressService.delete(addressService.findById(Long.parseLong(id)));
        return new ModelAndView(new RedirectView("/addresses"));
    }
}
