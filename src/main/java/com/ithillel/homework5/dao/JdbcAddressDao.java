package com.ithillel.homework5.dao;

import com.ithillel.homework5.entity.Address;
import com.ithillel.homework5.util.IdGenerator;
import com.ithillel.homework5.util.rowmapper.AddressRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class JdbcAddressDao implements AddressDao {
    private final JdbcTemplate jdbcTemplate;

    public JdbcAddressDao(JdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcTemplate.setDataSource(dataSource);
    }

    @Override
    public boolean save(Address address) {
        String query = "INSERT INTO homework_db.book.addresses (id, country, city, street, zip_code) " +
                "VALUES (?,?,?,?,?)";

        return jdbcTemplate.update(query,
                IdGenerator.generateId(jdbcTemplate, "SELECT MAX(id) FROM homework_db.book.addresses"),
                address.getCountry(),
                address.getCity(),
                address.getStreet(),
                address.getZipCode()) == 1;
    }

    @Override
    public Address findById(Long id) {
        String query = "SELECT * FROM homework_db.book.addresses WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new AddressRowMapper(), id);
    }

    @Override
    public List<Address> findAll() {
        String query = "SELECT * FROM homework_db.book.addresses";
        return jdbcTemplate.query(query, new AddressRowMapper());
    }

    @Override
    public boolean update(Address update) {
        String query = "UPDATE homework_db.book.addresses " +
                "SET country = ?, city = ?, street = ?, zip_code = ? WHERE id = ?";

        return jdbcTemplate.update(query,
                update.getCountry(),
                update.getCity(),
                update.getStreet(),
                update.getZipCode(),
                update.getId()) == 1;
    }

    @Override
    public boolean delete(Address address) {
        String query = "DELETE FROM homework_db.book.addresses WHERE id = ?";
        return jdbcTemplate.update(query, address.getId()) == 1;
    }
}
