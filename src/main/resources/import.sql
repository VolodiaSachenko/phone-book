create database homework_db;

create schema homework_db.book;

create table book.contacts
(
    id      bigint       not null
        constraint contacts_pk
            primary key,
    name    varchar(255) not null,
    surname varchar(255) not null,
    phone   varchar(255) not null
);

create table book.addresses
(
    id       bigint       not null
        constraint addresses_pk
            primary key,
    country  varchar(255) not null,
    city     varchar(255) not null,
    street   varchar(255) not null,
    zip_code varchar(255)
);

create table book.phone_book
(
    id         bigint not null
        constraint phone_book_pk
            primary key,
    address_id bigint not null
        constraint phone_books_addresses_null_fk
            references book.addresses
            on update cascade on delete cascade,
    contact_id bigint not null
        constraint phone_books_contacts_null_fk
            references book.contacts
            on update cascade on delete cascade
);

insert into book.addresses (id, country, city, street, zip_code)
values ('1', 'Ukraine', 'Київ', 'Хрещатик 9', '10289');

insert into book.addresses (id, country, city, street, zip_code)
values ('2', 'USA', 'New York', 'Regent street 9', '109874');

insert into book.addresses (id, country, city, street, zip_code)
values ('3', 'United Kingdom', 'London', 'Abbey Road 16', '894345');

insert into book.addresses (id, country, city, street, zip_code)
values ('4', 'Poland', 'Wroclaw', 'Jana Kazemira 18', '54213');

insert into book.addresses (id, country, city, street, zip_code)
values ('5', 'Germany', 'Berlin', 'Friedrichstrabe 1', '994321');

insert into book.contacts (id, name, surname, phone)
values ('1', 'Степан', 'Ковальчук', '+3809506634');

insert into book.contacts (id, name, surname, phone)
values ('2', 'Віктор', 'Тестовий', '+38044123875');

insert into book.contacts (id, name, surname, phone)
values ('3', 'Віктор', 'Тестовий', '+38044123875');

insert into book.contacts (id, name, surname, phone)
values ('4', 'Tony', 'Montana', '+256731294');

insert into book.contacts (id, name, surname, phone)
values ('5', 'Акіра', 'Куросава', '+7843129485');

insert into book.phone_book (id, address_id, contact_id)
values ('1', '1', '1');

insert into book.phone_book (id, address_id, contact_id)
values ('2', '1', '3');

insert into book.phone_book (id, address_id, contact_id)
values ('3', '2', '5');

insert into book.phone_book (id, address_id, contact_id)
values ('4', '2', '4');

insert into book.phone_book (id, address_id, contact_id)
values ('5', '2', '2');

insert into book.phone_book (id, address_id, contact_id)
values ('6', '2', '1');