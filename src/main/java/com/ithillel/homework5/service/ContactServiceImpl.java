package com.ithillel.homework5.service;

import com.ithillel.homework5.dao.JdbcContactDao;
import com.ithillel.homework5.entity.Contact;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {
    private final JdbcContactDao jdbcContactDao;

    public ContactServiceImpl(JdbcContactDao jdbcContactDao) {
        this.jdbcContactDao = jdbcContactDao;
    }

    @Override
    public boolean create(Contact contact) {
        return jdbcContactDao.save(contact);
    }

    @Override
    public Contact findById(Long id) {
        return jdbcContactDao.findById(id);
    }

    @Override
    public List<Contact> readAll() {
        return jdbcContactDao.findAll();
    }

    @Override
    public boolean update(Contact contact) {
        return jdbcContactDao.update(contact);
    }

    @Override
    public boolean delete(Contact contact) {
        return jdbcContactDao.delete(contact);
    }
}
