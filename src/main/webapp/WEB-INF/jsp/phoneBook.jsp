<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Phone Book</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <div class="container">
            <button type="button" class="btn btn-primary" onclick="location.href = '/phone-book/add'">Add</button>
        </div>
        <div>
            <h2 style="position: relative; left: 45%">Phone Book</h2>
            <table class="table table-striped">
                <tr>
                    <th>Address</th>
                    <th>Contact</th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach items="${phoneBook}" var="phoneB">
                    <tr>
                        <td>${phoneB.address.country}, ${phoneB.address.city}, ${phoneB.address.street}</td>
                        <td>${phoneB.contact.name} ${phoneB.contact.surname}, ${phoneB.contact.phoneNumber}</td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/phone-book/update/id${phoneB.id}">
                                <button type="submit" class="btn btn-outline-success">
                                    Edit
                                </button>
                            </form>
                        </td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/phone-book/delete/id${phoneB.id}">
                                <button type="submit" class="btn btn-danger">
                                    Remove
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>