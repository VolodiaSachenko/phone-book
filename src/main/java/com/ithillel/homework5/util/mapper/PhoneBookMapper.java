package com.ithillel.homework5.util.mapper;

import com.ithillel.homework5.entity.PhoneBook;
import com.ithillel.homework5.dto.AddressDto;
import com.ithillel.homework5.dto.ContactDto;
import com.ithillel.homework5.dto.PhoneBookDto;

public final class PhoneBookMapper {

    public static PhoneBookDto toDto(PhoneBook phoneBook, ContactDto contact, AddressDto address) {
        PhoneBookDto dto = new PhoneBookDto();
        dto.setId(phoneBook.getId());
        dto.setAddress(address);
        dto.setContact(contact);
        return dto;
    }

    public static PhoneBookDto toDtoUpdate(PhoneBook phoneBook) {
        PhoneBookDto dto = new PhoneBookDto();
        AddressDto addressDto = new AddressDto();
        ContactDto contactDto = new ContactDto();
        addressDto.setId(phoneBook.getAddress());
        contactDto.setId(phoneBook.getContact());
        dto.setId(phoneBook.getId());
        dto.setAddress(addressDto);
        dto.setContact(contactDto);
        return dto;
    }

    public static PhoneBook fromDto(PhoneBookDto phoneBookDto) {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.setId(phoneBookDto.getId());
        phoneBook.setAddress(phoneBookDto.getAddress().getId());
        phoneBook.setContact(phoneBookDto.getContact().getId());
        return phoneBook;
    }
}
