package com.ithillel.homework5.service;

import com.ithillel.homework5.entity.PhoneBook;

import java.util.List;

public interface PhoneBookService {
    boolean create(PhoneBook phoneBook);

    PhoneBook findById(Long id);

    List<PhoneBook> readAll();

    boolean update(PhoneBook update);

    boolean delete(PhoneBook phoneBook);
}
