package com.ithillel.homework5.controller;

import com.ithillel.homework5.service.AddressService;
import com.ithillel.homework5.service.ContactService;
import com.ithillel.homework5.service.PhoneBookService;
import com.ithillel.homework5.dto.AddressDto;
import com.ithillel.homework5.dto.ContactDto;
import com.ithillel.homework5.dto.PhoneBookDto;
import com.ithillel.homework5.util.mapper.AddressMapper;
import com.ithillel.homework5.util.mapper.ContactMapper;
import com.ithillel.homework5.util.mapper.PhoneBookMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class PhoneBookController {
    private final PhoneBookService phoneBookService;
    private final ContactService contactService;
    private final AddressService addressService;

    public PhoneBookController(PhoneBookService phoneBookService, ContactService contactService, AddressService addressService) {
        this.phoneBookService = phoneBookService;
        this.contactService = contactService;
        this.addressService = addressService;
    }

    @RequestMapping(value = {"/phone-book"}, method = RequestMethod.GET)
    public String getAll(Model model) {

        List<PhoneBookDto> dto = phoneBookService
                .readAll()
                .stream()
                .map(phoneBook -> {
                    ContactDto contactDto = ContactMapper.toDto(contactService.findById(phoneBook.getContact()));
                    AddressDto addressDto = AddressMapper.toDto(addressService.findById(phoneBook.getAddress()));
                    return PhoneBookMapper.toDto(phoneBook, contactDto, addressDto);
                })
                .collect(Collectors.toList());
        model.addAttribute("phoneBook", dto);
        return "phoneBook";
    }

    @RequestMapping(value = {"phone-book/add"}, method = RequestMethod.GET)
    public String addPhoneBookForm(Model model) {
        List<ContactDto> contacts = contactService.readAll()
                .stream()
                .map(ContactMapper::toDto)
                .collect(Collectors.toList());
        List<AddressDto> addresses = addressService.readAll()
                .stream()
                .map(AddressMapper::toDto)
                .collect(Collectors.toList());
        model.addAttribute("phoneBook", new PhoneBookDto());
        model.addAttribute("contacts", contacts);
        model.addAttribute("addresses", addresses);
        return "addPhoneBook";
    }

    @RequestMapping(value = {"phone-book/add/create"}, method = RequestMethod.POST)
    public ModelAndView createPhoneBook(@ModelAttribute("phoneBook") PhoneBookDto phoneBookDto) {

        phoneBookService.create(PhoneBookMapper.fromDto(phoneBookDto));
        return new ModelAndView(new RedirectView("/phone-book"));
    }

    @RequestMapping(value = {"phone-book/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {
        List<ContactDto> contacts = contactService.readAll()
                .stream()
                .map(ContactMapper::toDto)
                .collect(Collectors.toList());
        List<AddressDto> addresses = addressService.readAll()
                .stream()
                .map(AddressMapper::toDto)
                .collect(Collectors.toList());
        model.addAttribute("updatePhoneBook",
                PhoneBookMapper.toDtoUpdate(phoneBookService.findById(Long.parseLong(id))));
        model.addAttribute("contactsUpdate", contacts);
        model.addAttribute("addressesUpdate", addresses);
        return "updatePhoneBook";
    }

    @RequestMapping(value = {"phone-book/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updatePhoneBook(@ModelAttribute("updatePhoneBook") PhoneBookDto phoneBookDto) {

        phoneBookService.update(PhoneBookMapper.fromDto(phoneBookDto));
        return new ModelAndView(new RedirectView("/phone-book"));
    }

    @RequestMapping(value = {"phone-book/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deletePhoneBook(@PathVariable String id) {

        phoneBookService.delete(phoneBookService.findById(Long.parseLong(id)));
        return new ModelAndView(new RedirectView("/phone-book"));
    }
}
