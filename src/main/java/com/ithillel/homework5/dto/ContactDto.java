package com.ithillel.homework5.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ContactDto {
    private Long id;
    private String name;
    private String surname;
    private String phoneNumber;

    @Override
    public String toString() {
        return id.toString();
    }
}
