package com.ithillel.homework5.util.rowmapper;

import com.ithillel.homework5.entity.PhoneBook;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class PhoneBookRowMapper implements RowMapper<PhoneBook> {

    @Override
    public PhoneBook mapRow(ResultSet rs, int rowNum) throws SQLException {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.setId(rs.getLong("id"));
        phoneBook.setAddress(rs.getLong("address_id"));
        phoneBook.setContact(rs.getLong("contact_id"));

        return phoneBook;
    }
}
