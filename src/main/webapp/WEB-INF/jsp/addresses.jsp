<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Addresses</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <div class="container">
            <button type="button" class="btn btn-primary" onclick="location.href = '/address/add'">Add</button>
        </div>
        <div>
            <h2 style="position: relative; left: 45%">Addresses</h2>
            <table class="table table-striped">
                <tr>
                    <th>Country</th>
                    <th>City</th>
                    <th>Street</th>
                    <th>Zip code</th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach items="${addresses}" var="address">
                    <tr>
                        <td>${address.country}</td>
                        <td>${address.city}</td>
                        <td>${address.street}</td>
                        <td>${address.zipCode}</td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/address/update/id${address.id}">
                                <button type="submit" class="btn btn-outline-success">
                                    Edit
                                </button>
                            </form>
                        </td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/address/delete/id${address.id}">
                                <button type="submit" class="btn btn-danger">
                                    Remove
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>