package com.ithillel.homework5.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class PhoneBook {
    private Long id;
    private Long contact;
    private Long address;

    public PhoneBook() {
    }

    @Override
    public String toString() {
        return "PhoneBook{" +
                "id=" + id +
                ", contactInfo=" + contact +
                ", address=" + address +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneBook phoneBook = (PhoneBook) o;
        return id.equals(phoneBook.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
