package com.ithillel.homework5.dao;

import com.ithillel.homework5.entity.PhoneBook;
import com.ithillel.homework5.util.IdGenerator;
import com.ithillel.homework5.util.rowmapper.PhoneBookRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class JdbcPhoneBookDao implements PhoneBookDao {
    private final JdbcTemplate jdbcTemplate;

    public JdbcPhoneBookDao(JdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcTemplate.setDataSource(dataSource);
    }

    @Override
    public boolean save(PhoneBook phoneBook) {
        String query = "INSERT INTO homework_db.book.phone_book (id, address_id, contact_id) " +
                "VALUES (?,?,?)";

        return jdbcTemplate.update(query,
                IdGenerator.generateId(jdbcTemplate, "SELECT MAX(id) FROM homework_db.book.phone_book"),
                phoneBook.getAddress(),
                phoneBook.getContact()) == 1;
    }

    @Override
    public PhoneBook findById(Long id) {
        String query = "SELECT * FROM homework_db.book.phone_book WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new PhoneBookRowMapper(), id);
    }

    @Override
    public List<PhoneBook> findAll() {
        String query = "SELECT * FROM homework_db.book.phone_book";
        return jdbcTemplate.query(query, new PhoneBookRowMapper());
    }

    @Override
    public boolean update(PhoneBook update) {
        String query = "UPDATE homework_db.book.phone_book " +
                "SET address_id = ?, contact_id = ? WHERE id = ?";

        return jdbcTemplate.update(query,
                update.getAddress(),
                update.getContact(),
                update.getId()) == 1;
    }

    @Override
    public boolean delete(PhoneBook phoneBook) {
        String query = "DELETE FROM homework_db.book.phone_book WHERE id = ?";
        return jdbcTemplate.update(query, phoneBook.getId()) == 1;
    }
}
